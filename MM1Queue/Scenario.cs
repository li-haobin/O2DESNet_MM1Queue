﻿using MM1Queue.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue
{
    /// <summary>
    /// The Scenario class that specifies what to simulate
    /// </summary>
    public class Scenario : O2DESNet.Scenario
    {
        public Customer Customer { get; private set; }
        public Server Server { get; private set; }
        public Scenario(Customer customer, Server server)
        {
            Customer = customer;
            Server = server;
        }
    }
}
