﻿using MM1Queue.Statics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            var scenario = new Scenario(new Customer(hourlyArrivalRate: 4), new Server(hourlyServiceRate: 5));
            var status = new Status(scenario, seed: 0) { Display = false };
            var simulator = new Simulator(status);

            //while (true)
            //{
            //    simulator.Run(speed: 1000);
            //    System.Threading.Thread.Sleep(100);

            //    Console.Clear();
            //    Console.WriteLine("{0}\n-------------------",simulator.ClockTime);
            //    status.WriteToConsole();
            //}

            simulator.Run(TimeSpan.FromDays(365));

            Console.WriteLine("Ave. Q Length: {0}", status.QueueLengthCounter.AverageCount);
            Console.WriteLine("Ave. Cycle Time: {0} hrs", status.CustomersDeparted.Average(c => (c.EndServiceTime - c.ArriveTime).Value.TotalHours));
            Console.WriteLine("Server Util.: {0}", status.ServerCounter.AverageCount);
            Console.Write("\nPress any key to terminate...");
            Console.ReadKey();
        }
    }
}
