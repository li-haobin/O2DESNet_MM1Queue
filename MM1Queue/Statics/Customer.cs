﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Statics
{
    public class Customer
    {
        public double HourlyArrivalRate { get; private set; }
        public Customer(double hourlyArrivalRate) { HourlyArrivalRate = hourlyArrivalRate; }
    }
}
