﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Statics
{
    public class Server
    {
        public double HourlyServiceRate { get; private set; }
        public Server(double hourlyServiceRate) { HourlyServiceRate = hourlyServiceRate; }
    }
}
