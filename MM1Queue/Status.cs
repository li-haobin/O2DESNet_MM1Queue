﻿using MM1Queue.Dynamics;
using O2DESNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue
{
    /// <summary>
    /// The Status class that provides a snapshot of simulated system in run-time
    /// </summary>
    public class Status : Status<Scenario>
    {
        public int CountCustomers { get; set; }
        public Customer BeingServed { get; set; }
        public Queue<Customer> Queue { get; set; }
        public List<Customer> CustomersDeparted { get; set; }

        public HourCounter QueueLengthCounter { get; set; }
        public HourCounter ServerCounter { get; set; }

        public Status(Scenario scenario, int seed = 0) : base(scenario, seed)
        {
            CountCustomers = 0;
            BeingServed = null;
            Queue = new Queue<Customer>();
            CustomersDeparted = new List<Customer>();

            QueueLengthCounter = new HourCounter();
            ServerCounter = new HourCounter();
        }

        public override void WarmedUp(DateTime clockTime)
        {
            CustomersDeparted = new List<Customer>();
            QueueLengthCounter = new HourCounter();
            ServerCounter = new HourCounter();
        }

        public override void WriteToConsole()
        {
            Console.Write("In Queue: ");
            foreach (var customer in Queue) Console.Write("#{0} ", customer.Id);
            Console.WriteLine();
            if (BeingServed != null) Console.WriteLine("Being Served: #{0}", BeingServed.Id);
            else Console.WriteLine("Server is Idle.");
            Console.WriteLine("# Customers Departed: {0}", CustomersDeparted.Count);
        }
    }
}
