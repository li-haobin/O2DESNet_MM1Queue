﻿using MM1Queue.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Events
{
    /// <summary>
    /// O2DESNet Event - Arrive
    /// </summary>
    public class Arrive : O2DESNet.Event<Scenario, Status>
    {
        public Customer Customer { get; private set; }
        public Arrive(Customer customer)
        {
            Customer = customer;
        }

        public override void Invoke()
        {
            Log("{0}: Customer #{1} arrives.", ClockTime.ToLongTimeString(), Customer.Id);

            Customer.ArriveTime = ClockTime;
            if (Status.BeingServed == null) Execute(new StartService(Customer));
            else
            {
                Status.Queue.Enqueue(Customer);
                Status.QueueLengthCounter.ObserveChange(1, ClockTime);
            }
            Schedule(new Arrive(new Customer(++Status.CountCustomers)), TimeSpan.FromHours(MathNet.Numerics.Distributions.Exponential.Sample(DefaultRS, Scenario.Customer.HourlyArrivalRate)));
        }
    }
}
