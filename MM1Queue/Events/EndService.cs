﻿using MM1Queue.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Events
{
    /// <summary>
    /// O2DESNet Event - EndService
    /// </summary>
    public class EndService : O2DESNet.Event<Scenario, Status>
    {
        public Customer Customer { get; private set; }
        public EndService(Customer customer) { Customer = customer; }

        public override void Invoke()
        {
            Log("{0}: Customer #{1} departs.", ClockTime.ToLongTimeString(), Customer.Id);
            Customer.EndServiceTime = ClockTime;
            Status.CustomersDeparted.Add(Customer);
            Status.ServerCounter.ObserveChange(-1, ClockTime);

            if (Status.Queue.Count > 0)
            {
                Status.QueueLengthCounter.ObserveChange(-1, ClockTime);
                Execute(new StartService(Status.Queue.Dequeue()));
            }
            else Status.BeingServed = null;
        }
    }
}
