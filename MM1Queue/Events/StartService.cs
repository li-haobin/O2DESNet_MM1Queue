﻿using MM1Queue.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Events
{
    /// <summary>
    /// O2DESNet Event - StartService
    /// </summary>
    public class StartService : O2DESNet.Event<Scenario, Status>
    {
        public Customer Customer { get; private set; }
        public StartService(Customer customer) { Customer = customer; }

        public override void Invoke()
        {
            Customer.StartServiceTime = ClockTime;
            Status.BeingServed = Customer;
            Status.ServerCounter.ObserveChange(1, ClockTime);
            Schedule(new EndService(Customer), TimeSpan.FromHours(MathNet.Numerics.Distributions.Exponential.Sample(DefaultRS, Scenario.Server.HourlyServiceRate)));
        }
    }
}
