﻿using MM1Queue.Dynamics;
using MM1Queue.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue
{
    /// <summary>
    /// The simulator class
    /// </summary>
    public class Simulator : O2DESNet.Simulator<Scenario, Status>
    {
        public Simulator(Status status) : base(status)
        {
            Execute(new Arrive(new Customer(++Status.CountCustomers)));
        }
    }
}
