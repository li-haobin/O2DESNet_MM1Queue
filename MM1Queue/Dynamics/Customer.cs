﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MM1Queue.Dynamics
{
    public class Customer
    {
        public int Id { get; set; }
        public DateTime? ArriveTime { get; set; } = null;
        public DateTime? StartServiceTime { get; set; } = null;
        public DateTime? EndServiceTime { get; set; } = null;
        public Customer(int id) { Id = id; }
    }
}
